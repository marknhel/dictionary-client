var api = "https://api.dictionaryapi.dev/api/v2/entries/en/"

function populateSynonym(data, index, array){

	var synonym = $("<li>", {
		html: data
	});

	synonym.appendTo("fieldset:last .meanings .meaning:last .synonyms");

}

function populateAntonym(data, index, array){

	var antonym = $("<li>", {
		html: data
	});

	antonym.appendTo("fieldset:last .meanings .meaning:last .antonyms");

}


function populateDefinition(data, index, array){

	var definitionList = $("<li></li>")
	var definitionValue = $("<p>", {
		html: data.definition,
		"class": "definition"
	});
	var definitionExample = $("<p>", {
		html: data.example,
		"class": "example"
	});

	definitionList.appendTo("fieldset:last .meanings .meaning:last .definitions");
	definitionValue.appendTo("fieldset:last .meanings .meaning:last .definitions li:last");
	definitionExample.appendTo("fieldset:last .meanings .meaning:last .definitions li:last");

}

function populateMeaning(data, index, array){

	var partOfSpeech = $("<h2>", {
		html: data.partOfSpeech,
		"class": "partOfSpeech"
	});
	var meaning = $("<div>",{
		"class": "meaning"
	});
	var definitionText = $("<h3>", {
		html: "Definitions",
		"class": "definitionText"
	});
	var definitions = $("<ul>", {
		"class": "definitions"
	});

	partOfSpeech.appendTo("fieldset:last .meanings");
	meaning.appendTo("fieldset:last .meanings");
	definitionText.appendTo("fieldset:last .meanings .meaning:last");
	definitions.appendTo("fieldset:last .meanings .meaning:last");

	data.definitions.forEach(populateDefinition);


	if ( data.synonyms.length > 0 ){

		var synonymsText = $("<h3>", {
			html: "synonyms",
			"class": "synonymsText"
		});
		var synonyms = $("<ul>", {
			"class": "synonyms"
		});

		synonymsText.appendTo("fieldset:last .meanings .meaning:last");
		synonyms.appendTo("fieldset:last .meanings .meaning:last");
		data.synonyms.forEach(populateSynonym);
	}

	if ( data.antonyms.length > 0 ){

		var antonymsText = $("<h3>", {
			html: "antonyms",
			"class": "antonymsText"
		});
		var antonyms = $("<ul>", {
			"class": "antonyms"
		});

		antonymsText.appendTo("fieldset:last .meanings .meaning:last");
		antonyms.appendTo("fieldset:last .meanings .meaning:last");
		data.antonyms.forEach(populateAntonym);
	}

}

function populatePhoneticSound(sound){

	$("fieldset:last legend .phonetics").data("phoneticSound", { wordSound: sound });

}

function getPhoneticSound(phonetics){

	var sound;

	for( let i = 0; phonetics.length > i; i++){
		if( phonetics[i].audio){
			sound = phonetics[i].audio;
			break;
		}
	}

	return sound;
}

function populateWord(word, index, array){

	var fieldset = $("<fieldset class=\"word\"></fieldset>");
	var legend = $("<legend></legend>");
	var title = $("<span>", {
		html: word.word,
		"class": "title"
	});
	var phonetic = $("<span>" , {
		html: word.phonetic,
		"class": "phonetics",
	});
	var speakImage = $("<img>",{
		"class": "speak",
		"src": "./assets/img/speak.png",
		"alt": "Speak Word"
	});
	var meanings = $("<div>", {
		"class": "meanings"
	});


	var hr = $("<hr>");

	var soundLink = getPhoneticSound(word.phonetics);

	fieldset.appendTo("main");
	legend.appendTo("fieldset:last");
	title.appendTo("fieldset:last legend");

	phonetic.appendTo("fieldset:last legend");
	speakImage.appendTo("fieldset:last legend .phonetics");
	hr.appendTo("fieldset:last");
	meanings.appendTo("fieldset:last");

	if( word.phonetics.length > 0 ){
		$(".phonetics").show();
	}else{
		$(".phonetics").hide();
	}

	if(soundLink){
		$("fieldset:last legend .phonetics .speak").show();
	}
	else{
		$("fieldset:last legend .phonetics .speak").hide();
	}
	populatePhoneticSound(soundLink);

	word.meanings.forEach(populateMeaning);
}

function search(word){

	var query = api + word;

	$.get(query, function(data, status){
		data.forEach(populateWord);
	})
	.fail(function (data){
		var fieldset = $("<fieldset class=\"word\"></fieldset>");
		var legend = $("<legend></legend>");
		var title = $("<h1>",{
			html: "No Definitions Found",
			"class": "error-title"
		});
		var message = $("<p>", {
			html: "Sorry pal, we couldn't find definitions for the word you were looking for.",
			"class": "message"
		});
		var resolution = $("<p>",{
			html: "You can try the search again at later time or head to the web instead.",
			"class": "resolution"
		});

		fieldset.appendTo("main");
		legend.appendTo("main");
		title.appendTo(".word");
		message.appendTo(".word");
		resolution.appendTo(".word");
	});
}

$(document).on("click", ".phonetics", function (){

	var phoneticSoundLink = $(this).data("phoneticSound");

	if (!phoneticSoundLink){
		$(this).children("img").hide();

	}

	var sound = new Audio(phoneticSoundLink.wordSound);

	sound.play();
});

$(document).on("keypress", "#search", function (e){
	var key = e.which;
	if ( key == 13 ){
		var word = $("#search").val();
		$("main").empty();
		search(word);
	}
});

$(document).ready(function (){
	$("main").empty();
});
